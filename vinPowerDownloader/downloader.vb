﻿Imports System
Imports System.Security.Cryptography
Imports System.Configuration
Imports System.IO
Imports System.Net
Imports System.Text
Imports ICSharpCode.SharpZipLib.Zip

'there is a scheduled task in START >> CONTROL PANEL >> SCHEDULED TASKS that runs this
'build this and copy the .exe, the .exe.config and the sharpZipLib.dll from bin\release to \\63.96.132.106\c$\websites\STG_WebServices\vinPowerUpdater
Module downloader
    Private localDownloadPath = ConfigurationManager.AppSettings("VMDownloadLocation")
    Private localSavePath As String = ConfigurationManager.AppSettings("VMSaveLocation")

    Sub Main()
        Dim response As WebResponse = Nothing
        Dim stream As Stream = Nothing
        Dim request As WebRequest = Nothing
        Dim fs As FileStream = Nothing
        Dim streamIn As FileStream = Nothing
        Dim zipInStream As ZipInputStream = Nothing
        Dim entry As ZipEntry = Nothing
        Dim streamOut As FileStream = Nothing
        Dim buffer1(100000) As Byte
        Dim buffer2(100000) As Byte

        Try
            Dim url As String = "http://download.espdata.com/"
            Dim fileName As String = ConfigurationManager.AppSettings("fileName")
            Dim fullUrl As String = url & fileName
            Dim localDownloadFileName As String = localDownloadPath & fileName

            Console.WriteLine("Starting download for '" & fullUrl & "'")
            request = HttpWebRequest.Create(fullUrl)
            request.Credentials = New System.Net.NetworkCredential(ConfigurationManager.AppSettings("vpuser"), ConfigurationManager.AppSettings("vppass"))
            response = request.GetResponse
            stream = response.GetResponseStream

            Console.WriteLine("Reading stream.")
            File.Delete(localDownloadFileName)
            fs = File.Create(localDownloadFileName)

            While True
                Dim n As Integer = stream.Read(buffer1, 0, buffer1.Length)
                If n = 0 Then
                    Exit While
                Else
                    fs.Write(buffer1, 0, n)
                End If
            End While

            fs.Flush()
            fs.Close()

            stream.Close()
            response.Close()

            Console.WriteLine("Download complete.")


            Console.WriteLine("Beginning to un-zip file.")

            streamIn = New FileStream(localDownloadFileName, FileMode.Open, FileAccess.Read)
            zipInStream = New ZipInputStream(streamIn)
            entry = zipInStream.GetNextEntry

            File.Delete(localSavePath & entry.Name)
            streamOut = New FileStream(localSavePath & entry.Name, FileMode.Create, FileAccess.Write)

            Dim size As Integer = 100000

            While size > 0
                size = zipInStream.Read(buffer2, 0, buffer2.Length)
                streamOut.Write(buffer2, 0, size)
            End While

            streamIn.Flush()
            streamIn.Close()

            streamOut.Flush()
            streamOut.Close()

            zipInStream.Close()

            Console.WriteLine("Done un-zipping file.")
        Catch ex As Exception
            Console.WriteLine(ex.Message)
            Console.WriteLine("Sending email about error.")

            Try
                Dim msg As New System.Net.Mail.MailMessage
                msg.From = New System.Net.Mail.MailAddress(ConfigurationManager.AppSettings("fromEmail"))
                msg.To.Add(ConfigurationManager.AppSettings("errorEmails"))
                msg.Subject = "Vin Power Updater Error"
                msg.Body = ex.Message

                Dim smtp As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient()
                smtp.Send(msg)

                msg = Nothing
                smtp = Nothing
            Catch ex2 As Exception
            End Try
        Finally
            stream = Nothing
            response = Nothing
            request = Nothing
            fs = Nothing

            streamIn = Nothing

            zipInStream = Nothing
            entry = Nothing
            streamOut = Nothing

            buffer1 = Nothing
            buffer2 = Nothing
        End Try
    End Sub
End Module